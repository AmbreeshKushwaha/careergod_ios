//
//  APIManager.swift
//  TableDemo
//
//  Created by Shiv Kumar on 11/05/17.
//  Copyright © 2017 Shiv Kumar. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire


class APIManager: NSObject {
    
    
    
    //MARK: =====: SIGNUP Request Function :====
    class func requestSignup(_ url: String, params: [String : Any]?, headers: [String : String]?, completion:@escaping (JSON) -> Void, failure: @escaping (Error) -> Void){
        
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (responseObject) -> Void in
            
            print("Response ➤: ",responseObject)
            
            guard responseObject.result.isSuccess else{
                
                print("Error: Did not receive data")
                if responseObject.result.isFailure{
                    DispatchQueue.main.async {
                        let error : Error = responseObject.result.error!
                        failure(error)
                    }
                }
                return
            }
            DispatchQueue.main.async {
                let responseJson = JSON(responseObject.result.value!)
                completion(responseJson)
            }
        }
    }

    
    //MARK: =====: LOGIN Request Function :====
    class func requestLogin(_ url: String, params: [String :Any]?, headers: [String : String]?, completion:@escaping (JSON) -> Void, failure: @escaping (Error) -> Void){
        
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (responseObject) -> Void in
            
            print("Response ➤: ",responseObject)
            
            guard responseObject.result.isSuccess else{
                
                print("Error: Did not receive data")
                if responseObject.result.isFailure{
                    DispatchQueue.main.async {
                        let error : Error = responseObject.result.error!
                        failure(error)
                    }
                }
                return
            }
            DispatchQueue.main.async {
                let responseJson = JSON(responseObject.result.value!)
                completion(responseJson)
            }
        }
    }
    
    
    
    //MARK: =====: GET Request Function :====

    class func requestGET(_ url: String, headers: [String : Any]?, completion:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
        Alamofire.request(url).responseJSON { (responseObject) -> Void in
            
            print("Response ➤: ",responseObject)
            guard responseObject.result.isSuccess else{
                
                print("Error: Did not receive data")
                if responseObject.result.isFailure{
                    DispatchQueue.main.async {
                        let error : Error = responseObject.result.error!
                        failure(error)
                    }
                }
                return
            }
            DispatchQueue.main.async {
                let responseJson = JSON(responseObject.result.value!)
                completion(responseJson)
            }
        }
    }
    
    
   
    //MARK: =====: FORGOT PASSWORD Request Function :====
    class func requestForgotPassword(_ url: String, params: [String : Any]?, headers: [String : String]?, completion:@escaping (JSON) -> Void, failure: @escaping (Error) -> Void){
        
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (responseObject) -> Void in
            
            print("Response ➤: ",responseObject)
            
            guard responseObject.result.isSuccess else{
                print("Error: Did not receive data")
                if responseObject.result.isFailure{
                    DispatchQueue.main.async {
                        let error : Error = responseObject.result.error!
                        failure(error)
                    }
                }
                return
            }
            DispatchQueue.main.async {
                let responseJson = JSON(responseObject.result.value!)
                completion(responseJson)
            }
        }
    }
}


///: Unusable
/*if responseObject.result.isSuccess {
 let resJson = JSON(responseObject.result.value!)
 
 completion(resJson)
 }
 if responseObject.result.isFailure {
 let error : Error = responseObject.result.error!
 failure(error)
 }*/
