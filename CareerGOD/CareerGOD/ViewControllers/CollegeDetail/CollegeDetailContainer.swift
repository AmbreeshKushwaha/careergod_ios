//
//  CollegeDetailContainer.swift
//  CarrerGOD
//
//  Created by Devendra Singh on 5/6/17.
//  Copyright © 2017 FIITJEE. All rights reserved.
//

import UIKit
import CarbonKit

class CollegeDetailContainer: UIViewController, CarbonTabSwipeNavigationDelegate {
    
    var tabItems = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self .addHorizontalTabBar()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    func addHorizontalTabBar(){
        
        tabItems = ["HOME", "RANKING", "MAJORS", "APPLICATION REQUIRMENTS", "ADMISSION STATS", "COST & AID", "JOBS & INTERNSHIP", "CAMPUS INFO"]
        let carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: tabItems, delegate: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: self.view)
        
        carbonTabSwipeNavigation.toolbar.isTranslucent = false
        carbonTabSwipeNavigation.setNormalColor(UIColor.lightGray)
        carbonTabSwipeNavigation.toolbar.barTintColor = Constants.kThemeColour
        
        //carbonTabSwipeNavigation.setNormalColor(UIColor.init(red: 251/255.0, green: 234/255.0, blue: 225/255.0, alpha: 1.0), font: UIFont.init(name: "Roboto-Regular", size: 15)!)
        //carbonTabSwipeNavigation.setSelectedColor(UIColor.white, font: UIFont.init(name: "Roboto-Medium", size: 15)!)
        
        carbonTabSwipeNavigation.setTabBarHeight(40)
        carbonTabSwipeNavigation.setIndicatorColor(UIColor.white)
        
        
    }
    
    //MARK: CarbonKit Delegate
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        //let  storyBrd:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let chooseVC  = self.storyboard?.instantiateViewController(withIdentifier: "CollegeDetailVC") as! CollegeDetailVC
        chooseVC.indexNumber = Int(index)
        chooseVC.tabName = tabItems[Int(index)]

        /*
        switch index {
        case 0:
          
        case 1:
            
        case 2:
            
        case 3:
            
        case 4:
            
        default:
            break
        }*/
        
        
        return chooseVC
        
    }
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAt index: UInt) {
        /*
         switch index {
         case 0:
         
         case 1:
         
         case 2:
         
         case 3:
         
         case 4:
         
         default:
         break
         }*/
    }
    
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAt index: UInt) {
        print("Swipe Index:", index)
        
        //pageControllerRegistration.currentPage = Int(index)
        
    }
    


}
