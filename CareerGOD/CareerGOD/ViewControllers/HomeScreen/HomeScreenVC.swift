//
//  HomeScreenVC.swift
//  CarrerGOD
//
//  Created by Devendra Singh on 4/20/17.
//  Copyright © 2017 FIITJEE. All rights reserved.
//

import UIKit
import EventKit

class HomeScreenVC: UIViewController {

    @IBOutlet weak var tableViewHome: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        //let sing1 = Singleton.shared
        //let singl2 = Singleton.init()
        

        let sing1 = Singleton.sharedInstance()
        let singl2 = Singleton.sharedInstance()
        
        print(sing1.isStart, singl2)
        singl2.isStart = true
        print(sing1.isStart)
        
        /*do{
            try singl2.isStart
        } catch let error as NSError{
            print(error)
        }*/

        self .doTotal(num1: 5, num2: 49)
        
        self .addEventToCalendarAndFecth()
        
    }
    
    
    deinit {
        
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: UIButton Actions
    @IBAction func menuBtnTap(_ sender: AnyObject) {
        
        self.mm_drawerController.open(MMDrawerSide.left, animated: true, completion: nil)

    }
    
    
    func doTotal(num1 : Int?, num2: Int?) {
        //reduce  nested if statement and check positive way not negative way
        guard let fistNum = num1, num1! > 0 else{
            return
        }
        guard  let lastNum = num2, num2! < 50 else {
            return
        }
        // increase my scope which my variable accessible
        let total = fistNum + lastNum
        
        print(total)
        
    }
    
    func addEventToCalendarAndFecth() {
        
        let eventStore : EKEventStore = EKEventStore()
        
        // 'EKEntityTypeReminder' or 'EKEntityTypeEvent'
        
        eventStore.requestAccess(to: .event) { (granted, error) in
            
            if (granted) && (error == nil) {
                print("granted \(granted)")
                print("error \(error)")
                
                /*let event:EKEvent = EKEvent(eventStore: eventStore)
                 
                 event.title = "Test Title"
                 event.startDate = Date()
                 event.endDate = Date()
                 event.notes = "This is a note"
                 event.calendar = eventStore.defaultCalendarForNewEvents
                 
                 do {
                 try eventStore.save(event, span: .thisEvent)
                 } catch let error as NSError {
                 print("failed to save event with error : \(error)")
                 }
                 print("Saved Event")*/
                
                
                // What about Calendar entries?
                let startDate = NSDate().addingTimeInterval(-60*60*24)
                let endDate = NSDate().addingTimeInterval(60*60*24*3)
                let predicate2 = eventStore.predicateForEvents(withStart: startDate as Date, end: endDate as Date, calendars: nil)
                
                print("startDate:\(startDate) endDate:\(endDate)")
                
                let eV = eventStore.events(matching: predicate2) as [EKEvent]!
                
                if eV != nil {
                    for i in eV! {
                        print("Title  \(i.title)" )
                        print("stareDate: \(i.startDate)" )
                        print("endDate: \(i.endDate)" )
                        
                        if i.title == "Test Title" {
                            print("YES" )
                            // Uncomment if you want to delete
                            //eventStore.removeEvent(i, span: EKSpanThisEvent, error: nil)
                        }
                    }
                }
                
                
                
                
                // This lists every reminder
                let predicate = eventStore.predicateForReminders(in: [])
                eventStore.fetchReminders(matching: predicate) { reminders in
                    for reminder in reminders! {
                        print(reminder.title)
                    }
                }
                
                
                
            }
            else{
                
                print("failed to save event with error : \(error) or access not granted")
            }
        }
        
        
    }
    
}

//MARK: UITableView DataSource and Delegate
extension HomeScreenVC:UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        case 0:
            return 100
        case 1:
            return 200
        case 2:
            return 300
        case 3:
            return 250
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableCell", for: indexPath) as! SearchTableCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            return cell

        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpCommingTableCell", for: indexPath) as! UpCommingTableCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none

            return cell

        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpCommingTableCell", for: indexPath) as! UpCommingTableCell

            //let cell = tableView.dequeueReusableCell(withIdentifier: "RecommendedTableCell", for: indexPath) as! RecommendedTableCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none

            return cell

        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpCommingTableCell", for: indexPath) as! UpCommingTableCell

            //let cell = tableView.dequeueReusableCell(withIdentifier: "AdmissionTipsTableCell", for: indexPath) as! AdmissionTipsTableCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none

            return cell

        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "", for: indexPath)
            return cell

        }
        
    }
    
    
    
}

//MARK: UICollection DataSource and Delegate
extension HomeScreenVC:UICollectionViewDataSource, UICollectionViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView .dequeueReusableCell(withReuseIdentifier: "HomeUpcommingEventCell", for: indexPath) as! HomeUpcommingEventCell
        
        cell.contentView.backgroundColor = UIColor.gray
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        
        //self.gotoAppleCalendar(date: NSDate())
        
        let storyBrd = UIStoryboard(name: "Main", bundle: nil)
        let dtlContaineVC = storyBrd .instantiateViewController(withIdentifier: "CollegeDetailContainer") as! CollegeDetailContainer
        self.navigationController?.pushViewController(dtlContaineVC, animated: true)
        

    }

    func gotoAppleCalendar(date: NSDate) {
        let interval = date.timeIntervalSinceReferenceDate
        let url = NSURL(string: "calshow:\(interval)")!
        UIApplication.shared.openURL(url as URL)
    }
    
}

