//
//  SideMenuScreenVC.swift
//  CarrerGOD
//
//  Created by Devendra Singh on 4/21/17.
//  Copyright © 2017 FIITJEE. All rights reserved.
//

import UIKit

class SideMenuScreenVC: UIViewController {

    @IBOutlet weak var imageUserProfile: UIImageView!
    @IBOutlet weak var lblUserEmailId: UILabel!
    
    
    
    
    var sideMenuItems = [[String:String]]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        sideMenuItems = [
            ["titleName":"CarrerGod",
             "titleImage": ""
            ],
            ["titleName":"CarrerGod",
             "titleImage": ""
            ],
            ["titleName":"CarrerGod",
             "titleImage": ""
            ],["titleName":"CarrerGod",
               "titleImage": ""
            ],
              ["titleName":"CarrerGod",
               "titleImage": ""
            ],
              ["titleName":"CarrerGod",
               "titleImage": ""
            ]
        ]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func editProfileTap(_ sender: Any) {
    }
    

}

//MARK: UItableview Datasource and Delegate
extension SideMenuScreenVC:UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sideMenuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as! SideMenuCell
        
        cell.lblTitle.text = sideMenuItems[indexPath.row]["titleName"]! + " " + String(indexPath.row)
        //"\(String(describing: sideMenuItems[indexPath.row]["titleName"]!)) \(indexPath.row)"
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.mm_drawerController .closeDrawer(animated: true, completion: nil)
    }
    
}
