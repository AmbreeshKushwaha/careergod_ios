//
//  ViewController.swift
//  CareerGOD
//
//  Created by Devendra Singh on 5/16/17.
//  Copyright © 2017 Edfora Infotech Pvt. Ltd. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        self .signUp()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func signUp() {
        /*{device_type=2, password=123456, usertype=children, username=ambreesh, user_lat=0.0, device_udid=02:00:00:00:00:00, mobile=8299852456, email=ambreesh.kushwaha@edfora.com, device_token=dHJbL3mlzCA:APA91bEZChZzr4Y5ZCyAAxCNH-GdyUjH04VCxz_TLd-V0TzZ66GPq2bn66MJbZp72_TC-85In0oCwXm3-qXz8BVQ4fCbggFOUzqP7bHvNilOHVXI0oH5GLwzk7oMv1XXm4Flh5zkTDKP, confirm_password=123456, user_long=0.0}
         */
        
        
        var postParams = [String : Any]()
        
        postParams ["device_type"] = Constants.kDeviceType
        postParams ["password"] = "123456"
        postParams ["confirm_password"] = "123456"
        postParams ["usertype"] = "children"
        postParams ["username"] = "ambreesh1"
        //postParams ["firstname"] = "ambreesh2"
        postParams ["user_lat"] = "28.5355"
        postParams ["user_long"] = "77.3910"
        postParams ["device_udid"] = "02:00:00:00:00:10"
        postParams ["mobile"] = "8299852156"
        postParams ["email"] = "ambreesh1.kushwaha@edfora.com"
        postParams ["device_token"] = "dHJbL3mlzCA:APA91bEZChZzr4Y5ZCyAAxCNH-GdyUjH04VCxz_TLd-V0TzZ66GPq2bn66MJbZp72_TC-85In0oCwXm3-qXz8BVQ4fCbggFOUzqP7bHvNilOHVXI0oH5GLwzk7oMv1XXm4Flh5zkTDZP"

        print("postParams", postParams)
        //["Content-Type" : "application/json"]
        APIManager .requestSignup(Constants.kBASEURL + Constants.kAPIRegistration, params: postParams, headers: ["" : ""], completion: { (JSON) in
        print(JSON)
       }) { (Error) in
        print(Error.localizedDescription)
        }
        
    }
    
    
    func resendOTP() {
        
    }
    
    
    func navigateToHomeScreenWithMMDrawerController() {
        
        let mainStoryBrd:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let centerViewController = mainStoryBrd.instantiateViewController(withIdentifier: "HomeScreenVC") as! HomeScreenVC
        
        let leftViewController = mainStoryBrd.instantiateViewController(withIdentifier: "SideMenuScreenVC") as! SideMenuScreenVC
        
        let centerContainer: MMDrawerController = MMDrawerController(center: centerViewController, leftDrawerViewController: leftViewController)
        
        let menuWidth:CGFloat = UIScreen.main.bounds.size.width * 0.8
        centerContainer.setMaximumLeftDrawerWidth(menuWidth, animated: false, completion: nil)
        
        //centerContainer.openDrawerGestureModeMask = MMOpenDrawerGestureMode.panningCenterView
        centerContainer.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.all
        
        
        self.navigationController?.pushViewController(centerContainer, animated: false)
        
        
    }

}

