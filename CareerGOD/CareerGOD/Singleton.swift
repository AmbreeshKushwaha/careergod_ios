//
//  Singleton.swift
//  CarrerGOD
//
//  Created by Devendra Singh on 4/21/17.
//  Copyright © 2017 FIITJEE. All rights reserved.
//

import Foundation


final class Singleton {
    
    private init() {
        
        // print("constructor...");
    }
    
    //MARK: Shared Instance
    private static let sharedObject : Singleton = Singleton()
    
    public static func sharedInstance() -> Singleton {
        
        // print("Instance()")
        return sharedObject
    }
    
    //MARK: Local Variable
    var isStart = false ;
    
    
    //MARK: - Location Update
    var currLatitude:String!
    var currLongitude:String!
    
    
    }
















//final class Singleton1 {
//    
//    // Can't init is singleton
//    private init() {
//    
//    
//    }
//    
//    //MARK: Shared Instance
//    
//    static let shared: Singleton1 = Singleton1()
//    
//    
//    //MARK: Local Variable
//    
//    var emptyStringArray : [String] = []
//    
//}

//class Singleton: NSObject {
//    
//    // MARK: - Shared Instance
//    
//    static let sharedInstance: Singleton = {
//        let instance = Singleton()
//        // setup code
//        return instance
//    }()
//    
//    // MARK: - Initialization Method
//    
//    override init() {
//        
//        
//        //super.init()
//    }
//}
