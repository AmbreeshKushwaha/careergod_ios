//
//  Constants.swift
//  CarrerGOD
//
//  Created by Devendra Singh on 4/21/17.
//  Copyright © 2017 FIITJEE. All rights reserved.
//

import Foundation

//API's
//let kBaseURL = NSURL(string: "http://www.example.com/")
//var manager = AFHTTPRequestOperationManager(baseURL: kBaseURL)


//Define Constants
struct Constants {
    
    //MARK: Theme Color
    static let kThemeColour = UIColor(red: 93/255.0, green: 115/255.0, blue: 10/255.0, alpha: 1.0)

    //MARK: Base URL & API names
    static let kBASEURL              = "http://dev.careergod.com/api/"
    static let kAPIRegistration     = "user-register.json"
    static let kAPIResndOTP         = "resend_otp.json"
    
    //MARK: SystemPreferences Items
    
    
    //MARK:
    static let kDeviceType = "1" //1 for iOS, 2 for Android

    //static let kRupeeSymbel = "\u{20B9}"
    
    
    
}
